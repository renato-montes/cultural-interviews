Cultural Interviews
===================

This project contains the data for the Cultural Interviews project, after
the website it was hosted on (wikispaces.com) was taken down.

The `old/wikipages/` subdirectory contains the data of the pages in raw
Mediawiki format as obtained in the evening of Sunday, July 29th 2018.
The `old/authors/` subdirectory contains lists of the authors of each page
as extracted from the wikipage histories on Sunday, July 29th 2018. The `html/`
subdirectory contains HTML files generated from the `old/` data at first, then
modified with more recent updates.

The website is currently available at [ser-be-etre-shi.neocities.org](https://ser-be-etre-shi.neocities.org/cultural-interviews)

All the files in the `old/` and `html/` folders are published under the CC-BY-NC
version 3 license. This means anybody is free to modify and repost the data
presented here, as long as the various authors are publicly listed and no money
is made off the data. The owners of the copyright are many, and these are listed
in COPYRIGHT-OWNERS.txt. See LICENSE.txt for the details of the license.

