//Cultural Interviews with Turkish Executives is a compilation of over 180 brief video clips in which Turkish executives discuss cultural issues that are of interest to North Americans. Over 30 native Turkish professionals offer their opinion on these questions. The objective of the interviews is three-fold: First, provide practical cultural information. The opinions represent those of real people. At times they are even contradictory, but they are designed to be a catalyst for discussion, not to provide a definitive answer about some stereotype. Second, the interviews provide vocabulary in areas within a professional setting. The interviews present diverse vocabulary within the context of each individual&#039;s comments. Third, these materials provide non-native speakers of Turkish with multiple examples of natural speech, illustrating the way that speakers really talk.//

//Click on any of the topics below and you will be taken to a page with the corresponding videos. Each youtube video is accompanied by a transcript and English translation, and you may also turn on/off the subtitles as desired.//

|| **Working w/ North Americans**
1. [[TK Initial Fact-Finding Phase|Initial fact-finding phase]]
2. Win-win situations
3. [[TK Reacting to Initial Resistance|Reacting to initial resistance]]
4. [[TK Reformulating Strategies|Reformulating strategies]]
5. [[TK Ratification From Superiors|Ratification from superiors]]
6. [[TK Follow-Up After Negotiations|Follow-up after negotiations]]
7. [[TK Logistical and Emotional Needs|Logistical and emotional needs]]
8. International vs. corporate style
9. Stereotype: Pushy American
10. Stereotype: Short-term focused
11. [[TK Limited Cultural Knowledge|Stereotype: Limited cultural knowledge]]
12. Stereotype: Always in a hurry
13. Stereotype: Litigious Nature || **Professional Activities**
1. [[Tk Communication Style|Communication Style]]
2. [[TK Type of Office|Type of office]]
3. [[TK Government and Politics|Government &amp; politics]]
4. [[TK Ability versus Connections|Ability vs. connections]]
5. [[TK Race, Color, and Gender|Race, color, and gender]]
6. Women executives
7. Loyalty to self vs. Company
8. [[TK Working in Groups|Working in groups]]
9. Responsibility to society vs. investors
10. Role of lawyers
11. [[TK Getting Side-Tracked|Getting side-tracked]]
12. [[TK Putting Things in Writing|Putting things in writing]]
13. [[TK Mixing Business and Pleasure|Mixing business and pleasure]] || **Language Issues**
1. [[TK Which Language to Speak|Which language to speak]]
2. Buy in any language, sell in theirs
3. [[TK Use of Interpreters|Use of interpreters]]
4. Saying, &#039;I don&#039;t know&#039;
5. [[TK Saying &#039;Yes&#039; or &#039;No&#039;|Saying, &#039;Yes&#039; or &#039;No&#039;]]
6. [[TK Avoid Offending Others|Avoid offending others]] ||
|| **Social Situations**
1. [[TK Formal and Informal Language|Formal and informal language]]
2. [[TK Greeting Others|Greeting others]]
3. [[TK Gift Giving|Gift giving]]
4. [[TK Non-Drinkers and Vegetarians|Non-drinkers and vegetarians]]
5. [[TK Invitations to a Home|Invitations to a home]]
6. [[TK Clothing and Dress|Clothing and dress]]
7. [[TK Names and Titles|Names and titles]]
8. Academic degrees
9. [[TK Professional versus Personal Life|Professional vs. personal life]]
10. Machismo || **Time and Scheduling**
1. [[TK Business and Food|Business and food]]
2. Agendas
3. [[TK Time is Money|Time is money]]
4. Normal daily schedule
5. Meal times and business ||